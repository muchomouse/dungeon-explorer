﻿using Dungeon_Explorer.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dungeon_Explorer.Game
{
    class Item : IEquipment
    {
        #region Members
        string mType;
        int mWeight;

        #endregion
        #region IItem Overrides
        public override int Weight { get { return mWeight; } }
        public override string GetKey()
        {
            throw new NotImplementedException();
        }
        public override int GetLevel()
        {
            throw new NotImplementedException();
        }
        public override string GetName()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
