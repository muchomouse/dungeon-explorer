﻿using Dungeon_Explorer.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dungeon_Explorer.Game
{
    /*
    public class Quip :IQuip
    {
        #region Members
        string mQuiptext;
        string mAction;
        string mContext;
        QuipType mType;
        public QuipType Type { get { return mType; } }
        #endregion
        #region Constructor
        public Quip(string Action, string Context, string Quiptext)
        {
            mAction = Action;
            mContext = Context;
            mQuiptext = Quiptext;
            mType = QuipType.STATIC;
        }
        public Quip(string Action, string Context, string Quiptext,QuipType Type)
        {
            mAction = Action;
            mContext = Context;
            mQuiptext = Quiptext;
            mType = Type;
        }
        #endregion
        #region IQuip Overrides
        public override string Action
        {
            get { return mAction; }
        }
        public override string Context
        {
            get { return mContext; }
        }
        public override string Quiptext
        {
            get { return mQuiptext; }
        }
        #endregion
        #region Enumerations
        public enum QuipAction
        {
            GO = 0,
            TAKE = 1,
            DROP = 2,
            KILL = 3,
            LOOK = 4,
        }
        public enum QuipContext
        {
            DuplicatItem = 0

        }
        public enum QuipType
        {
            DYNAMIC,
            STATIC,
        }
        #endregion
    }
    */
}
