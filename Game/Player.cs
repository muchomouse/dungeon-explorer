﻿using Dungeon_Explorer.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dungeon_Explorer.Game

{
    class Player : IPlayer
    {
        #region Members
        string mName = "Shantanu";
        int mCapacity;
        Dictionary<string, string> mInventory = new Dictionary<string, string>();
        public Dictionary<string, string> Inventory { get { return mInventory; } }
        #endregion
        #region IPlayer Overrides
        public override int Capacity 
        {
            get { return mCapacity; } 
        }
        public override string GetName()
        {
            return mName;
        }
        public override bool IsInInventory(string itemToTest)
        {
            string myItem = null;
            mInventory.TryGetValue(itemToTest, out myItem);
            if (myItem != null)
                return true;
            return false;
        }

        #endregion
    }
}
