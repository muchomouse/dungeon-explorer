﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dungeon_Explorer.Interface;
using System.Xml.Serialization;
using System.Xml;


namespace Dungeon_Explorer.Game
{
    public class Room : IRoom , IXmlSerializable
    {
        #region Members
        [Flags]
        public enum RoomFalgs
        {
            None = 0,
            Enter = 1,
            Exit = 2,
        }
        public string mKey;
        RoomFalgs mFlags;
        string mName;
        string mDescription;
       
        Dictionary<string,string> mNeighbors = new Dictionary<string,string>();
        public Dictionary<string,string> Neighbors { get { return mNeighbors; } }
        
        Dictionary<string, string> mItems = new Dictionary<string, string>();
        public Dictionary<string,string> Items { get { return mItems;} }

        Dictionary<string, string> mMonsters = new Dictionary<string, string>();
        public Dictionary<string, string> Monsters { get { return mMonsters; } }

        #endregion
        #region Constructors
        /// <summary>
        /// Manual constructor for default maze
        /// </summary>
        /// <param name="uniqueKey">unique name for the state flags</param>
        /// <param name="flags">flags to indicate enter and exit nodes</param>
        /// <param name="name">name to show the user</param>
        /// <param name="description">description to show the user</param>
        /// <param name="paths">string of comma separated paths</param>
        /// <param name="neighbors">string of comma separated neighbors</param>
        public Room(string uniqueKey, RoomFalgs flags, string name, string description, string paths, string neighbors)
        {
            mKey = uniqueKey;
            mFlags = flags;
            mName = name;
            mDescription = description;
            string[] pathArray = paths.Split(',');
            string[] neighborsArray = neighbors.Split(',');
            for (int i = 0; i < pathArray.Length; i++)
            {
                Neighbors.Add(pathArray[i], neighborsArray[i]);
                i++;
            }
        }
        /// <summary>
        /// Constructor to create an object from a save file
        /// </summary>
        /// <param name="reader">xml stream to read from</param>
        public Room(XmlReader reader)
        {
            ReadXml(reader);
        }
        #endregion
        #region Helper Methods
        public bool IsStartState { get { return (mFlags & RoomFalgs.Enter) != RoomFalgs.None; } }
        public bool IsExitState { get { return (mFlags & RoomFalgs.Exit) != RoomFalgs.None; } }
        public string Key { get { return mKey; } }
        public bool IsMyName(string nameToTest)
        {
            if (nameToTest.ToLower() == mName.ToLower())
                return true;
            if (nameToTest.ToLower() == mKey.ToLower())
                return true;
            return false;
        }
        public bool IsPath(string pathToTest)
        {
            string testPath = null;
            mNeighbors.TryGetValue(pathToTest, out testPath);
            if (testPath == null || testPath == "")
                return false;
            return true;
        }
        public bool IsItem(string itemToTest)
        {
            try
            {
                string test = mItems[itemToTest];
                return true;
            }
            catch (KeyNotFoundException)
            {
                return false;
            }
        }
        public bool IsMonster(string MonsterToTest)
        {
            try
            {
                string test = mMonsters[MonsterToTest];
                return true;
            }
            catch (KeyNotFoundException)
            {
                return false;
            }
        }

        #endregion
        #region IRoom Overrides
        public override string GetName()
        {
            return mName;
        }
        public override void Run()
        {
            Console.WriteLine();
            Console.WriteLine(mDescription);
            if(mItems.Count > 0)
                Console.WriteLine("There are also these items:");
            foreach (var item in mItems)
            {
                Console.WriteLine("- {0}", item.Value);
            }
            if (mMonsters.Count > 0)
                Console.WriteLine("There are also these monsters:");
            foreach (var monster in mMonsters)
            {
                Console.WriteLine("- {0}", monster.Value);
            }

        }
        #endregion
        #region IXmlaserialazable Members
        public System.Xml.Schema.XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(System.Xml.XmlReader reader)
        {
            reader.ReadStartElement();
            mKey = reader.ReadElementContentAsString("UniqueName", "");
            string flagString = reader.ReadElementContentAsString("Flags", "");
            mFlags = (RoomFalgs)Enum.Parse(typeof(RoomFalgs), flagString);
            mName = reader.ReadElementContentAsString("VisibleName", "");
            mDescription = reader.ReadElementContentAsString("Description", "");
            try
            {
                while (true)
                {
                    if (reader.Name == "Neighbor" && reader.NodeType == XmlNodeType.Element)
                    {
                        mNeighbors.Add(reader.GetAttribute("uniquePath"), reader.ReadString());
                        reader.ReadEndElement();
                    }
                    else if (reader.Name == "Item" && reader.NodeType == XmlNodeType.Element)
                    {
                        mItems.Add(reader.GetAttribute("uniqueName"), reader.ReadString());
                        reader.ReadEndElement();
                    }
                    else if (reader.Name == "Monster" && reader.NodeType == XmlNodeType.Element)
                    {
                        mMonsters.Add(reader.GetAttribute("uniqueName"), reader.ReadString());
                        reader.ReadEndElement();
                    }
                    else
                    {
                        reader.ReadEndElement(); // </Room>
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Cought Exception:");
                switch (reader.Name)
                {
                    case "Neighbor":
                        Console.Write("Maybe a duplicate \"path\" in Neighbor");
                        break;
                    case "Item":
                        Console.Write("Maybe a duplicate \"name\" in Neighbor");
                        break;
                    case "Monster":
                        Console.Write("Maybe a duplicate \"name\" in Monster");
                        break;
                    default:
                        Console.WriteLine(ex.Message);
                        break;
                }
                Console.WriteLine();
                Console.WriteLine("Press enter to terminate the application");
                Console.ReadLine();
                Environment.Exit(0);
            }
            
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteElementString("UniqueName", mKey);
            writer.WriteElementString("Flags", mFlags.ToString());
            writer.WriteElementString("VisibleName", mName);
            writer.WriteElementString("Description", mDescription);
            foreach (var n in mNeighbors)
            {
                writer.WriteStartElement("Neighbor");
                writer.WriteAttributeString("uniquePath", n.Key);
                writer.WriteString(n.Value);
                writer.WriteEndElement();
            }
            foreach (var i in mItems)
            {
                writer.WriteStartElement("Item");
                writer.WriteAttributeString("uniqueName", i.Key);
                writer.WriteString(i.Value);
                writer.WriteEndElement();
            }
            foreach (var m in mMonsters)
            {
                writer.WriteStartElement("Monster");
                writer.WriteAttributeString("uniqueName", m.Key);
                writer.WriteString(m.Value);
                writer.WriteEndElement();

            }
        }
        #endregion
    }
}
