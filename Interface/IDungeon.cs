﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dungeon_Explorer.Interface
{
    public abstract class IDungeon
    {
        // Accessor to look at the current room.
        public abstract IRoom CurrentRoom { get; }

        // Accessor to look at the available actions
        public abstract Dictionary<string, string> Actions { get; } 

        // Is this an action?
        public abstract bool IsAction(string action);

        // Prase the user input to determin if this is something you can do here
        public abstract Dictionary<string,string> ParseUserInput(string userInput);

        // Advanc to named room, return true on success.
        public abstract bool Advance(string nextRoom);

        // Is this room a "completion" room" Are we There yet?
        public abstract bool IsComplete();
    }
}
