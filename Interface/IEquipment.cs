﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dungeon_Explorer.Interface
{
    public abstract class IEquipment
    {
        // Accessor to get item weight
        public abstract int Weight { get; }

        // Utility method to help us display useful string
        public abstract string GetKey();

        // Utility method to help with another useful string
        public abstract string GetName();
        
        // Utility method to get type
        //public abstract string GetType();

        //Utilty method to get item level
        public abstract int GetLevel();

        // Helpful string when we need it for something
        public override string ToString()
        {
            return GetName();
        }
    }
}
