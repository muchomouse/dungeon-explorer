﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dungeon_Explorer.Interface
{
    public abstract class IRoom
    {
        // Utility method to help us dispaly useful string
        public abstract string GetName();

        // Do something
        public abstract void Run();

        // Helpful string when we need it for something
        public override string ToString()
        {
            return GetName();
        }
    }
}
