﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dungeon_Explorer.Interface
{
    public abstract class IQuip
    {
        // Accessor for the quip
        public abstract string Quiptext { get; }

        // Accessor for action
        public abstract string Action { get; }

        // Accessor for context
        public abstract string Context { get; }
    }
}
