﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dungeon_Explorer.Interface
{
    public abstract class IPlayer
    {
        // Accessor to get capacity
        public abstract int Capacity { get; }

        // Utility method to get name
        public abstract string GetName();

        // checks inventory
        public abstract bool IsInInventory(string Item);
    }
}
