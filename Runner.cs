﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dungeon_Explorer.Interface;
using Dungeon_Explorer.Game;

namespace Dungeon_Explorer
{
    class Runner
    {
        #region Main
        static void Main(string[] args)
        {
            // First we make a dungeon
            Dungeon myDungeon = new Dungeon();

            // We have a dungeon, lets run it
            while (!myDungeon.IsComplete())
            {
                // Tell us where we are
                myDungeon.CurrentRoom.Run();

                // enter Command Loop
                bool changeRoom = false;
                while (!changeRoom)
                {
                    IRoom thisRoom = myDungeon.CurrentRoom;

                    //request input from the users
                    Console.Write("> ");
                    string userInput = Console.ReadLine();
                    Dictionary<string,string> theWords = new Dictionary<string,string>();
                    theWords = myDungeon.ParseUserInput(userInput);

                    switch (theWords["theAction"])
                    {
                        case "go":
                            if (theWords["theDirection"] != null)
                            {
                                bool isRoom = myDungeon.Advance(theWords["theDirection"]);
                                if (isRoom)
                                    changeRoom = true;
                                else
                                    Console.WriteLine("There is a door there, but behind it is just all white\nwith the text: 404 Room not found");
                            }
                            else
                            {
                                Console.WriteLine("Something went wrong");
                            }
                            
                           
                            break;
                        case "take":
                            if (theWords["theObject"] != null)
                                myDungeon.TakeItem(theWords["theObject"]);
                            else
                                Console.WriteLine("Take what?");
                            break;
                        case "drop":
                            if (theWords["theImplement"] != null)
                                myDungeon.DropItem(theWords["theImplement"]);
                            else
                                Console.WriteLine("Drop what?");
                            break;
                        case "look":
                            myDungeon.CurrentRoom.Run();
                            break;
                        case "kill":
                            if (theWords["theMonster"] != null)
                                myDungeon.KillMonster(theWords["theMonster"], theWords["theImplement"]); // the Implement can be null
                            else
                                Console.WriteLine("Kill what?");
                            break;
                        case "exit":
                            Environment.Exit(0);
                            break;
                        default:
                            Console.WriteLine("Don't know how to \"{0}\"", userInput.Trim());
                            break;
                    }
                }
            }
        }
        #endregion
    }
}
